/*
 * TestSet.java
 *
 * Created on May 19, 2007, 5:16 PM
 *
 * Author: Tomasz Gebarowski
 */

package nn;

/**
 * Neural Netoworks Simulator Class Used for Storing Test Sets
 */
public class TestSet {
    
    /* Number of inputs */
    private int nIn;
    
    /* Number of outputs */
    private int nOut;
    
    /* Number of items in set */
    private int count;
    
    /* Vector Used for storing sets */
    private java.util.Vector<Set> sets;
  
    /* Single Set helper Class */
    private class Set {
        
        private int nIn;
        private int nOut;
        
        private double[] inputs;
        private double[] outputs;
        
        public Set( int nIn, int nOut ) {
            inputs = new double[nIn];
            outputs = new double[nOut];
        }
    }
    
    /** Creates a new instance of TestSet */
    public TestSet(int nIn, int nOut) {
        this.nIn = nIn;
        this.nOut = nOut;
        count = 0;
        sets = new java.util.Vector();
    }
    
    /** Add net Train Data Set
     *  @param inputs[] double Array of inputs
     *  @param outputs[] double Array of doubles
     */
    public void addTestSet(double []inputs, double []outputs ) {
        Set s = new Set(nIn, nOut);
        s.inputs = inputs;
        s.outputs = outputs;
        sets.add( s );
        count++;
    }
   
    /** Get number of items in set
     * @return int Number of items in set
     */
    public int getCount() {
        return this.count;
    }
  
    /** Get input at given position 
     * @param pos int Input position
     * @return double[] Inputs at given position
     */
    public double[] getInputsAt(int pos) {
        return ((Set)sets.get(pos)).inputs;
    }

    
    /** Get input at given position 
     * @param pos int Output position
     * @return double[] Outputs at given position
     */    
    public double[] getOutputsAt(int pos) {
        return ((Set)sets.get(pos)).outputs;
    }
    
}
