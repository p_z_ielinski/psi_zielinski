/*
 * NeuralTrainer.java
 *
 * Created on 5 maj 2007, 15:43
 *
 * Author: Tomasz Gebarowski
 */

package nn;

/**
 * Neural Netowork Interface defining basic operations
 */
public interface NeuralTrainer {
    public double trigger(double val);
    public void trainNetwork(double[] target);  
    public void performTraining(int steps, TestSet ts);
}
