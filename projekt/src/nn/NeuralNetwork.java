/*
 * NeuralNetwork.java
 *
 * Created on 5 maj 2007, 15:22
 *
 * Author: Tomasz Gebarowski
 */

package nn;
import java.lang.Math;
/**
 * Neural Netowork Basic Class
 */
public abstract class NeuralNetwork extends Object implements NeuralTrainer {
    
    /* Neural Network Settings */
    protected int numberOfInputs;
    protected int numberOfOutputs;
    protected int numberOfHiddens;
    
    /* Speed of learning */
    protected double learningRate;
    
    /* Arrays for storing neural network values */
    public double[] inputs;
    public double[] outputs;
    public double[] hiddens;
    
    /* Arrays used for storing coefficients */
    public double[][] hiddenToOutputWeights;
    public double[][] inputToHiddenWeights;
    
    /* Used for storing coefficient error */
    public double error;
    
    
    
    /** Creates a new instance of NeuralNetwork
     *  @param nInputs int Number of inputs
     *  @param nHiddens int Number of hidden fields
     *  @param nOutputs int Number of output fields
     */
    public NeuralNetwork(int nInputs, int nHiddens, int nOutputs) {
        this.numberOfInputs = nInputs;
        this.numberOfHiddens = nHiddens;
        this.numberOfOutputs = nOutputs;
        
        this.inputs = new double[nInputs+1];
        
        // Set Bias       
        this.hiddens = new double[nHiddens+1];
        this.outputs = new double[nOutputs];
        
        this.inputs[nInputs] = 1.0;
        this.hiddens[nHiddens] = 1.0;
       
        
        this.hiddenToOutputWeights = new double[nHiddens+1][nOutputs];
        this.inputToHiddenWeights = new double[nInputs+1][nHiddens];
        
        // Set default learning rate
        this.learningRate = 0.3;
        
        // Set random weights
        randomWeights();
        
    }
    
    /** Generate random weights for hiddenToOutput & inputToHidden coefficients within range <-2;2> */
    private void randomWeights() {
        for ( int i = 0; i < numberOfInputs+1; i++ ) 
            for ( int j = 0; j < numberOfHiddens; j++ ) 
                inputToHiddenWeights[i][j] = Math.random() * 4.0 - 2.0; // random weight between -2 and 2 
        
        for ( int i = 0; i < numberOfHiddens+1; i++ )  {
            for ( int j = 0; j < numberOfOutputs; j++ ) {
                hiddenToOutputWeights[i][j] = Math.random() * 4.0 - 2.0;         
            }
        }
        
        
    }
    
    
    /** Get number of inputs
     * @return double Number of inputs
     */
    public double getNumberOfInputs() {
        return this.numberOfInputs;
    }

    /** Get number of outputs
     * @return double Number of outputs
     */
    public double getNumberOfOutputs() {
        return this.numberOfOutputs;
    }

    /** Get number of hiddens
     * @return double Number of hiddens
     */
    public double getNumberOfHiddens() {
        return this.numberOfHiddens;
    }
    /** Get Error
     * @return double error
     */    
    public double getError() {
        return this.error;
    }
   
    /** Forwards input through the whole neural network structure and stores output in output array
     *  @param input double[] Array of inputs
     */
    public void processForward( double[] input ) {
        
        for ( int i = 0; i < numberOfInputs; i++)
        {
            inputs[i] = input[i];
        }
        
        for ( int i = 0; i < numberOfHiddens; i++ ) {
            double sum = 0.0;
            
            for ( int j = 0; j < numberOfInputs; j++ ) {
                sum += inputs[j] * inputToHiddenWeights[j][i];
            }
            hiddens[i] = trigger(sum);
        }
        
        for ( int i = 0; i < numberOfOutputs; i++ ) {
            double sum = 0.0;
            for ( int j = 0; j < numberOfHiddens; j++ ) {
                sum += hiddens[j] * hiddenToOutputWeights[j][i];
            }
            
            outputs[i] = trigger(sum);
        }
     
        
    }
}
