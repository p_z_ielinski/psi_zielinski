/*
 * KnowledgeBase.java
 *
 * Created on May 25, 2007, 12:28 PM
 *
 * Author: Tomasz Gebarowski
 */

package kb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import nn.TestSet;

/**
 * Digit Recognizer Neural Network Self Learning Tool
 * Knowledge Base Class
 */
public class KnowledgeBase {
    
    
    /** Class responsible for storing data for a single digit structue */
    private class NeuralCaseData {
        
        // Pixel representation of digit
        private double[] inputs;
        // Literal value
        private String value;
        
        // Simple constructor
        public NeuralCaseData(double[] inputs, String value ) {
            this.inputs = inputs;
            this.value = value; 
        }
        
    }
    
    // Default Width & Height of digit
    private int width = 0;
    private int height = 0;
    
    // Number of outputs ( determines possibility of representing up to 2^4 different symbols )
    private static final int numberOfDigits = 4;
    
    // Reference to TestSet class
    private TestSet knowledgeBase;
    
    /** Creates a new instance of KnowledgeBase */
    public KnowledgeBase() {
    }
    
    
    /** Method responsible for loading data from a given directory.
     *  Directory contains all the digit pixel representation data used for learing
     *  @param cat String Direcory used for reading 
     */ 
    public void load(String cat) {
       
        File f = new File("kb/" + cat + "/");
        
        // Load file header
        loadFileHeader(new File("kb/" + cat + "/0.ns"));
             
        // Create and initalize knowledge base
        knowledgeBase = new TestSet( width * height , numberOfDigits);
        
        // Create filter
        FileFilter ff = new FileFilter() {
            
            // Accepty only files with .ns extension
            public boolean accept( File f ) {
                String name = f.getName();
                String extension = name.substring(name.indexOf(".")+1);
                
                if ( extension.equals("ns") )
                    return true;
                
                return false;
            }     
            // Override method used for printing filter description
            public String getDescription() {
                return "Nerual Source File";
            }
        };
        
        // List files from given directory
        File[] fileList = f.listFiles(ff);
        
        try {
            for ( int i = 0; i < fileList.length; i++ ) {

                    // Current file name
                    String currentFile = fileList[i].getCanonicalPath();
                    
                    // Read single digit pixel representation data file
                    NeuralCaseData nd = this.readNerualSourceFile(currentFile);
                    // Add this data to knowledge base
                    knowledgeBase.addTestSet(nd.inputs, binaryDigit(nd.value) );
                    
                    
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        }  
    }
    
    /** Method responsible for loading file header
     *  @param f File Reference to opened file
     */
    private void loadFileHeader(File f ) {
        try {  
            FileReader fr     = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);

            // Get width & height from file
            width = Integer.valueOf( br.readLine()).intValue();
            height = Integer.valueOf( br.readLine()).intValue();
            
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /** Method used for reading data from single .ns file
     *  @param fileName String Path to the .ns file
     *  @return NerualCaseData Reference to newly created NeuralCaseData object
     */
    public NeuralCaseData readNerualSourceFile(String fileName) { 

        String record = null;
        int recCount = 0; 
        

        String characterVal = "";
        
        double[] data;

        try { 

	   FileReader fr     = new FileReader(fileName);
           BufferedReader br = new BufferedReader(fr);

           record = new String();
           
           // Read header info
           width = Integer.valueOf( br.readLine()).intValue();
           height = Integer.valueOf( br.readLine()).intValue();
           characterVal = br.readLine();
           
           // Create buffer for pixel data
           data = new double[width*height];
           
           // Read data line by line
           while ((record = br.readLine()) != null) {
               
              for ( int i = 0; i < record.length(); i++ ) {  
                  char ch = record.charAt(i);
                  String str = String.valueOf(ch);
                  data[recCount] = Double.parseDouble( str );  
                  recCount++;
              } 
           } 
           
           // Create new  NeuralCaseData object
           NeuralCaseData nd = new NeuralCaseData(data, characterVal );
           return nd;
           
        } catch (IOException e) { 
           // catch possible io errors from readLine()
           System.out.println("Uh oh, got an IOException error!");
           e.printStackTrace();
        }
        return null;
        
    }
    
    /** Return Knowledge Base
     *  @return TestSet Knowledge base representation
     */
    public TestSet getKB() {
        return knowledgeBase;
    }
    
    /** Return number of inputs. Number of pixels creating digit
     *  @return int Number of inputs
     */
    public int getNumberOfInputs() {
        return width * height;
    }
    
    /** Return number of outputs. Number of pixels creating digit
     *  @return int Number of inputs
     */
    public int getNumberOfOutputs() {
        return this.numberOfDigits;
    }
    
    /** Convert string representation of decimal to binary number ( array representation )
     *  @param d String Decimal in a string form
     *  @return double[] Array binary representation of decimal
     */
    public double[] binaryDigit( String d ) {
        
        Integer digit = Integer.valueOf( d );
        
        double output[] = new double[numberOfDigits];
        
        
        for( int i = 0; i < numberOfDigits; i++ ) {
            int rest = digit % 2;
            digit = digit / 2;
            output[i] = rest;        
        }        
        return output;
    }
}
